const fs = require('fs');
const chalk = require('chalk');

let year = 2018;
let day = 1;
let last = false, oneDay = false;

const args = process.argv.slice(2);
if (args[0] === 'last')
  last = true;
if (args[0] === 'day' && parseInt(args[1]) >= 1 && parseInt(args[1]) <= 25) {
  day = parseInt(args[1]);
  oneDay = true;
}

let solver, arr;

while(true) {
    const path = `./${year}/${("0" + day).slice(-2)}`;
    if (!fs.existsSync(path)) {
        if (last && solver && arr) {
          console.log(chalk.bold(`${year}-12-${("0" + (day - 1)).slice(-2)}`));
          console.log(" ", chalk.cyan(solver.one(arr)));
          console.log(" ", chalk.green(solver.two(arr)));
        }
        console.log(chalk.red('\nALL DONE'));
        break;
    }
    solver = require(path + '/solution');
    arr = fs.readFileSync(path + '/input.txt')
      .toString()
      .split('\n')
      .map(s => s.replace(/\r$/, ''))
      .filter(s => s.length > 0);
    if (!last) {
      console.log(chalk.bold(`${year}-12-${("0" + day).slice(-2)}`));

      let solutions = fs.readFileSync(path + '/correct.txt')
        .toString()
        .split('\n')
        .map(s => s.replace(/\r$/, ''));

      let time = process.hrtime();
      let first = solver.one(arr);
      time = process.hrtime(time);
      time = (time[0] + time[1] / 1e9) * 1000;
      if (year === 2018 && day === 10) {
        console.log(' ', chalk.cyan(first), chalk.gray(`${solutions[0]}?`), time, 'ms');
      } else {
        console.log(' ', chalk.cyan(first), ' ', '' + first === solutions[0] ? chalk.green('✓') : chalk.red('✘'), time, 'ms');
      }
      time = process.hrtime();
      let second = solver.two(arr);
      time = process.hrtime(time);
      time = (time[0] + time[1] / 1e9) * 1000;
      console.log(' ', chalk.blue(second), ' ', '' + second === solutions[1] ? chalk.green('✓') : chalk.red('✘'), time,  'ms');
      if (oneDay) {
        break;
      }
    }
    day++;
    if (day === 26) {
      day = 1;
      year++;
    }
}