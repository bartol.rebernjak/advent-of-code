const sortBy = require('lodash/sortBy');

let directions = ["left", "up", "right", "down"];
//let turns = ["left", "straight", "right"];


function  getNext(x, y, direction) {
  switch (direction) {
    case "right":
      return [1, 0];
    case "left":
      return [-1, 0];
    case "up":
      return [0, -1];
    case "down":
      return [0, 1];
  }
}

function bounce(direction, symbol) {
  if (symbol === '/') {
    switch (direction) {
      case "right":
        return "up";
      case "left":
        return "down";
      case "up":
        return "right";
      case "down":
        return "left";
    }
  }
  if (symbol === '\\') {
    switch (direction) {
      case "left":
        return "up";
      case "right":
        return "down";
      case "down":
        return "right";
      case "up":
        return "left";
    }
  }
}

function crashed(carts) {
  for (let i = 0; i < carts.length; i++) {
    for (let j = i + 1; j < carts.length; j++) {
      if (carts[i][0] === carts[j][0] && carts[i][1] === carts[j][1])
        return carts[i][0] + ',' + carts[i][1];
    }
  }
  return false;
}

function clearCrashed(carts) {
  let toClear = [];
  for (let i = 0; i < carts.length; i++) {
    for (let j = i + 1; j < carts.length; j++) {
      if (carts[i][0] === carts[j][0] && carts[i][1] === carts[j][1]) {
        toClear.push(i);
        toClear.push(j);
      }
    }
  }
  for (let index of sortBy(toClear).reverse()) {
    carts.splice(index, 1);
  }
  return carts;
}

function one(input) {
  let carts = [];
  let map = [];
  for (let i = 0; i < input.length; i++) {
    let row = [];
    for (let j = 0; j < input[i].length; j++) {
      if (['<', '^', '>', 'v'].includes(input[i][j])) {
        carts.push([j, i, ['<', '^', '>', 'v'].indexOf(input[i][j]), 0]); // x, y, direction, next turn
        row.push(['-', '|', '-', '|'][['<', '^', '>', 'v'].indexOf(input[i][j])]);
      } else  {
        row.push(input[i][j]);
      }
    }
    map.push(row);
  }
  while (true) {
    carts = sortBy(carts, c => c[1] * 1000 + c[0]);
    for (let cart of carts) {
      cart[0] += getNext(cart[0], cart[1], directions[cart[2]])[0];
      cart[1] += getNext(cart[0], cart[1], directions[cart[2]])[1];
      if (['/', '\\'].includes(map[cart[1]][cart[0]])) {
        cart[2] = directions.indexOf(bounce(directions[cart[2]], map[cart[1]][cart[0]]));
      }
      if (map[cart[1]][cart[0]] === '+') {
        cart[2] = ((cart[2] + cart[3] - 1) % 4 + 4) % 4;
        cart[3] = (cart[3] + 1) % 3;
      }
      if (crashed(carts)) {
        return crashed(carts);
      }
    }
  }
}


function two(input) {
  let carts = [];
  let map = [];
  for (let i = 0; i < input.length; i++) {
    let row = [];
    for (let j = 0; j < input[i].length; j++) {
      if (['<', '^', '>', 'v'].includes(input[i][j])) {
        carts.push([j, i, ['<', '^', '>', 'v'].indexOf(input[i][j]), 0]); // x, y, direction, next turn
        row.push(['-', '|', '-', '|'][['<', '^', '>', 'v'].indexOf(input[i][j])]);
      } else  {
        row.push(input[i][j]);
      }
    }
    map.push(row);
  }
  while (carts.length > 1) {
    let cartsCopy = carts.slice(0);
    carts = sortBy(carts, c => c[1] * 1000 + c[0]);
    for (let cart of carts) {
      cart[0] += getNext(cart[0], cart[1], directions[cart[2]])[0];
      cart[1] += getNext(cart[0], cart[1], directions[cart[2]])[1];
      if (['/', '\\'].includes(map[cart[1]][cart[0]])) {
        cart[2] = directions.indexOf(bounce(directions[cart[2]], map[cart[1]][cart[0]]));
      }
      if (map[cart[1]][cart[0]] === '+') {
        cart[2] = ((cart[2] + cart[3] - 1) % 4 + 4) % 4;
        cart[3] = (cart[3] + 1) % 3;
      }
      cartsCopy = clearCrashed(cartsCopy);
    }
    carts = cartsCopy;
  }
  return (carts[0][0] +  ',' + carts[0][1]);
}

module.exports = {one, two};