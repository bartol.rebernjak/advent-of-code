const isEqual = require('lodash/isEqual');

function addr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] + values[b];
  return values;
}

function addi(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] + b;
  return values;
}

function mulr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] * values[b];
  return values;
}

function muli(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] * b;
  return values;
}

function banr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] & values[b];
  return values;
}

function bani(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] & b;
  return values;
}

function borr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] | values[b];
  return values;
}

function bori(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a] | b;
  return values;
}

function setr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = values[a];
  return values;
}

function seti(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = a;
  return values;
}

function gtir(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = +(a > values[b]);
  return values;
}

function gtri(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = +(values[a] > b);
  return values;
}

function gtrr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = +(values[a] > values[b]);
  return values;
}

function eqir(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = +(a === values[b]);
  return values;
}

function eqri(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = +(values[a] === b);
  return values;
}

function eqrr(registers, a, b, c) {
  let values = registers.slice(0);
  values[c] = +(values[a] === values[b]);
  return values;
}

function extractRegisters(line) {
  let numberRegex = /\d+/g;
  return [numberRegex.exec(line)[0], numberRegex.exec(line)[0], numberRegex.exec(line)[0], numberRegex.exec(line)[0]].map(Number);
}

function extractParameters(line) {
  return line.split(' ').slice(1).map(Number);
}

function extractOpCode(line) {
  return parseInt(line.split(' ')[0]);
}

function one(input) {
  let operations = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr];
  let totalCounter = 0;
  for (let i = 0; i < input.length; i += 3) {
    if (input[i].startsWith('Before')) {
      let correctCounter = 0;
      for (let op of operations) {
        if (isEqual(op(extractRegisters(input[i]), ...extractParameters(input[i + 1])), extractRegisters(input[i + 2]))) {
          correctCounter += 1;
        }
      }
      if (correctCounter >= 3) {
        totalCounter += 1;
      }
      continue;
    }
    break;
  }
  return totalCounter;
}


function two(input) {
  let operations = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr];
  let i;
  let dict = {};
  for (let j = 0; j < 16; j++) {
    if (!dict[j]) {
      dict[j] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    }
    for (i = 0; i < input.length; i += 3) {
      if (!input[i].startsWith('Before')) {
        break;
      }
      if (!isEqual(operations[j](extractRegisters(input[i]), ...extractParameters(input[i + 1])), extractRegisters(input[i + 2])) && dict[j].includes(extractOpCode(input[i + 1]))) {
        dict[j].splice(dict[j].indexOf(extractOpCode(input[i + 1])), 1);
      }
    }
  }
  while (Object.keys(dict).reduce((acc, k) => acc + dict[k].length, 0) > 16) {
    for (let op of Object.keys(dict)) {
      if (dict[op].length === 1) {
        for (let ops of Object.keys(dict)) {
          if (dict[ops].includes(dict[op][0]) && ops !== op) {
            dict[ops].splice(dict[ops].indexOf(dict[op][0]), 1);
          }
        }
      }
    }
  }
  let codeToOp = {};
  for (let op of Object.keys(dict)) {
    codeToOp[dict[op][0]] = op;
  }
  let registers = [0, 0, 0, 0];
  for (i; i < input.length; i++) {
    registers = operations[codeToOp[extractOpCode(input[i])]](registers, ...extractParameters(input[i]));
  }
  return registers[0];
}

module.exports = {one, two};
