const range = require('lodash/range');

function getGuardId (entry) {
  let guardIdRegex = /#(\d+)/;
  let match = guardIdRegex.exec(entry);
  if (match)
    return parseInt(match[1]);
  return null;
}

function getMinutes (entry) {
  let minuteRegex = /:(\d\d)]/;
  return parseInt(minuteRegex.exec(entry)[1]);
}

function one(input) {
  input.sort();
  let dict = {}, guard = -1, fell = 0;
  for (let entry of input) {
    if (getGuardId(entry)) {
      guard = getGuardId(entry);
      if (!(guard in dict))
        dict[guard] = [];
    } else if (entry.includes("falls")) {
      fell = getMinutes(entry);
    } else {
      dict[guard] = dict[guard].concat(range(fell, getMinutes(entry)));
    }
  }
  let max = 0, maxGuard;
  for (let guard of Object.keys(dict)) {
    if (dict[guard].length > max) {
      maxGuard = guard;
      max = dict[guard].length;
    }
  }
  let frequencies = dict[maxGuard].reduce((acc, curr) => {
    if (curr in acc) {
      acc[curr]++;
    } else {
      acc[curr] = 1;
    }
    return acc;
  }, {});
  let maxMinute = Object.keys(frequencies).reduce((a, b) => frequencies[a] > frequencies[b] ? a : b);
  return parseInt(maxGuard) * maxMinute;
}

function two(input) {
  input.sort();
  let dict = {}, guard = -1, fell = 0;
  for (let entry of input) {
    if (getGuardId(entry)) {
      guard = getGuardId(entry);
      if (!(guard in dict))
        dict[guard] = [];
    } else if (entry.includes("falls")) {
      fell = getMinutes(entry);
    } else {
      dict[guard] = dict[guard].concat(range(fell, getMinutes(entry)));
    }
  }
  let dict2 = {};
  for (let guard of Object.keys(dict)) {
    for (let minute of dict[guard]) {
      if (!(minute in dict2))
        dict2[minute] = {};
      if (!(guard in dict2[minute]))
        dict2[minute][guard] = 1;
      else dict2[minute][guard]++;
    }
  }
  let max = 0, multiple;
  for (let minute of Object.keys(dict2)) {
    for (let guard of Object.keys(dict2[minute])) {
      if (dict2[minute][guard] > max) {
        max = dict2[minute][guard];
        multiple = parseInt(minute) * parseInt(guard);
      }
    }
  }
  return multiple;
}

module.exports = {one, two};