function one(input) {
  let scoreboard = [3, 7];
  let elf1 = 0, elf2 = 1;
  let limit = parseInt(input[0]);
  while (scoreboard.length < limit + 10) {
    scoreboard.push(...(scoreboard[elf1] + scoreboard[elf2]).toString().split('').map(Number));
    elf1 = (elf1 + (scoreboard[elf1] + 1)) % scoreboard.length;
    elf2 = (elf2 + (scoreboard[elf2] + 1)) % scoreboard.length;
  }
  return scoreboard.slice(limit, scoreboard.length).join('');
}


//TODO indecently slow but it's probably just me or js being bad
function two(input) {
  let scoreboard = [3, 7];
  let elf1 = 0, elf2 = 1;
  let pattern = input[0], pSize = pattern.length;
  while (true) {
    scoreboard.push(...(scoreboard[elf1] + scoreboard[elf2]).toString().split('').map(Number));
    elf1 = (elf1 + (scoreboard[elf1] + 1)) % scoreboard.length;
    elf2 = (elf2 + (scoreboard[elf2] + 1)) % scoreboard.length;
    if (scoreboard.length > pSize) {
      if (scoreboard.slice(scoreboard.length - pSize, scoreboard.length).join('') === pattern)
        return scoreboard.length - pSize;
      if (scoreboard.slice(scoreboard.length - pSize - 1, scoreboard.length - 1).join('') === pattern) {
        return scoreboard.length - pSize - 1;
      }
    }
  }
}

module.exports = {one, two};