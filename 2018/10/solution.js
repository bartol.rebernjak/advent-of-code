function calculateArea(stars) {
  let maxX = Math.max(...stars.map(s => s[0]));
  let minX = Math.min(...stars.map(s => s[0]));
  let maxY = Math.max(...stars.map(s => s[1]));
  let minY = Math.min(...stars.map(s => s[1]));
  return (maxX - minX) * (maxY - minY);
}

function advance(stars, reverse=false) {
  for (let star of stars) {
    star[0] += reverse ? -star[2] : star[2];
    star[1] += reverse ? -star[3] : star[3];
  }
  return stars;
}

function draw(stars) {
  let maxX = Math.max(...stars.map(s => s[0]));
  let minX = Math.min(...stars.map(s => s[0]));
  let maxY = Math.max(...stars.map(s => s[1]));
  let minY = Math.min(...stars.map(s => s[1]));
  let map = [];
  for (let i = 0; i <= maxY - minY; i++) {
    let row = new Array(maxX - minX + 1).fill(' ');
    map.push(row);
  }
  for (let star of stars) {
    map[star[1] - minY][star[0] - minX] = '*';
  }
  for (let row of map) {
    row[row.length - 1] += '\n';
    row[0] = ' ' + row[0];
  }
  map[0][0] = map[0][0].substr(1);
  return map.map(r => r.join('')).join('');
}

function one(input) {
  let stars = [];
  for (let line of input) {
    let digitRegex = /-*\d+/g;
    stars.push([digitRegex.exec(line)[0], digitRegex.exec(line)[0], digitRegex.exec(line)[0], digitRegex.exec(line)[0]].map(Number));
  }
  let oldArea, newArea;
  oldArea = newArea = calculateArea(stars);
  while (oldArea >= newArea) {
    oldArea = newArea;
    advance(stars);
    newArea = calculateArea(stars);
  }
  advance(stars, true);
  return draw(stars);
}

function two(input) {
  let stars = [];
  for (let line of input) {
    let digitRegex = /-*\d+/g;
    stars.push([digitRegex.exec(line)[0], digitRegex.exec(line)[0], digitRegex.exec(line)[0], digitRegex.exec(line)[0]].map(Number));
  }
  let oldArea, newArea, counter = 0;
  oldArea = newArea = calculateArea(stars);
  while (oldArea >= newArea) {
    oldArea = newArea;
    advance(stars);
    newArea = calculateArea(stars);
    counter ++;
  }
  return counter - 1;
}

module.exports = {one, two};