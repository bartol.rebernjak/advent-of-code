const sum = require('lodash/sum');

function one(input) {
  return sum(input.map(Number));
}

//TODO takes almost 10 sec
/*function two(input) {
  let frequencies = new Set(), freq = 0, counter = 0;

  while (!frequencies.has(freq))  {
    frequencies.add(freq);
    freq = freq + input.map(Number)[counter];
    counter = ++counter % input.length;
  }
  return freq;
}*/

function two(input) {
  input = input.map(Number);
  let partialSums = [input[0]];
  let i = 0, seen = {}, index;
  while (true) {
    if (partialSums[i] in seen) {
      index = seen[partialSums[i]];
      break;
    }
    seen[partialSums[i]] = i;
    i++;
    partialSums.push(partialSums[partialSums.length - 1] + input[i % input.length]);
  }
  return partialSums[index];
}

module.exports = {one, two};