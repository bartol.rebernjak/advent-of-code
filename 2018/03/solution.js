function one(input) {
  const parsed = parse(input);
  let arr = [];
  for (let claim of parsed) {
    for (let i = claim.x; i < claim.x + claim.width; i++) {
      for (let j = claim.y; j < claim.y + claim.height; j++) {
        arr.push(`${i}-${j}`);
      }
    }
  }
  arr.sort();
  let counter = 0;
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] === arr[i + 1] && arr[i - 1] && arr[i] !== arr[i - 1])
      counter++;
  }
  return counter;
}

function two(input) {
  const parsed = parse(input);
  let arr = [];
  for (let claim of parsed) {
    for (let i = claim.x; i < claim.x + claim.width; i++) {
      for (let j = claim.y; j < claim.y + claim.height; j++) {
        arr.push(`${i}-${j}`);
      }
    }
  }
  let set = new Set(arr);
  arr.sort();
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] === arr[i + 1] && arr[i - 1] && arr[i] !== arr[i - 1])
      set.delete(arr[i]);
  }
  loop:
  for (let claim of parsed) {
    for (let coordinate of getCoords(claim)) {
      if (!set.has(coordinate)) {
        continue loop;
      }
    }
    return claim.id;
  }
}


function getCoords(claim) {
  let arr = [];
  for (let i = claim.x; i < claim.x + claim.width; i++) {
    for (let j = claim.y; j < claim.y + claim.height; j++) {
      arr.push(`${i}-${j}`);
    }
  }
  return arr;
}

function parse(input) {
  let parsed = [];
  for (let line of input) {
    parsed.push({
      "id" : parseInt(line.substr(line.indexOf("#") + 1, line.indexOf("@") - 1)),
      "x" : parseInt(line.substr(line.indexOf("@") + 1, line.indexOf(",") - 1)),
      "y" : parseInt(line.substr(line.indexOf(",") + 1, line.indexOf(":") - 1)),
      "width" : parseInt(line.substr(line.indexOf(":") + 1, line.indexOf("x") - 1)),
      "height" : parseInt(line.substr(line.indexOf("x") + 1, line.length))
    })
  }
  return parsed;
}

module.exports = {one, two};