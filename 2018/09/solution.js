const maxBy = require('lodash/maxBy');

// >1d for 7M marbles
/*function calculate(input, factor) {
  let last = parseInt(input[0].split(' ')[6]) * factor;
  let scores = new Array(parseInt(input[0].split(' ')[0])).fill(0);
  let circle = [];
  for (let i = 0; i <= last; i++) {
    if (i % 23 !== 0 || i === 0) {
      circle.splice(2, 0, i);
      circle = circle.concat(circle.slice(0, circle.indexOf(i)));
      circle.splice(0, circle.indexOf(i));
    } else {
      let count = circle.length;
      scores[i % scores.length] += +i;
      scores[i % scores.length] += +circle.splice(count - 7, 1);
      circle = circle.concat(circle.slice(0, count - 7));
      circle.splice(0, count - 7);ł
    }
  }
  return maxBy(scores);
}*/

// <1s for 7M marbles
function calculateBetter(input, factor) {
  let last = parseInt(input[0].split(' ')[6]) * factor;
  let scores = new Array(parseInt(input[0].split(' ')[0])).fill(0);
  let current = {
    id: 0
  };
  current.prev = current;
  current.next = current;
  for (let i = 1; i <= last; i++) {
    if (i % 23 !== 0) {
      current = insertAfter(i, current.next);
    } else {
      scores[i % scores.length] += i;
      current = current.prev.prev.prev.prev.prev.prev;
      scores[i % scores.length] += current.prev.id;
      current.prev.prev.next = current;
      current.prev = current.prev.prev;
    }
  }
  return maxBy(scores);
}

function insertAfter(id, marble) {
  let toBeAdded = {
    id: id,
    prev: marble,
    next: marble.next
  };
  marble.next.prev = toBeAdded;
  marble.next = toBeAdded;
  return toBeAdded;
}


function one(input) {
  return calculateBetter(input, 1);
}

function two(input) {
  return calculateBetter(input, 100);
}

module.exports = {one, two};