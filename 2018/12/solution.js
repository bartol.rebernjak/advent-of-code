function count(state, offset) {
  let counter = 0;
  for (let i = 0; i < state.length; i++) {
    if(state[i] === '#') {
      counter += i - offset;
    }
  }
  return counter;
}

function removeEmpty(state) {
  let regexStart = /^\.+/, regexEnd = /\.+$/;
  state = state.replace(regexStart, '');
  state = state.replace(regexEnd, '');
  return state;
}

function advance(state, producers) {
  let newState = [];
  for (let j = 0; j < state.length - 4; j++) {
    newState.push(producers.has(state.substr(j, 5)) ? '#' : '.');
  }
  return newState.join('');
}

function one(input) {
  let state = input[0].split(' ')[2];
  let producers = new Set();
  for (let line of input.slice(2)) {
    if (line.split(' ')[2] === '#') {
      producers.add(line.split(' ')[0]);
    }
  }
  let offset = 0;
  for (let i = 1; i <= 20; i++) {
    state = '....' + state + '....';
    state = advance(state, producers);
    offset = offset + 2;
  }
  return count(state, offset);
}

function two(input) {
  let state = input[0].split(' ')[2];
  let producers = new Set();
  for (let line of input.slice(2)) {
    if (line.split(' ')[2] === '#') {
      producers.add(line.split(' ')[0]);
    }
  }
  let offset = 0;
  let newState;
  while (true) {
    state = '....' + state + '....';
    newState = advance(state, producers);
    offset = offset + 2;
    if (removeEmpty(state) === removeEmpty(newState)) {
      return count(state, offset) + (count(advance(state, producers), offset - 2) - count(state, offset)) * (50000000000 - (offset / 2 + 1));
    }
    state = newState;
  }
}

module.exports = {one, two};