const countBy = require('lodash/countBy');

function one(input) {
  let counts = [0, 0];

  for (let id of input) {
    [2, 3].forEach((n) => {
      if (Object.values(countBy(id)).includes(n)) {
        counts[n - 2]++;
      }
    });
  }
  return counts[0] * counts[1];
}

function two(input) {
  let id1, id2;
  for (id1 of input) {
    for (id2 of input) {
      for (let i = 0; i < id1.length - 1; i++)  {
        if (id1.substr(0, i) + id1.substr(i + 1) === id2.substr(0, i) + id2.substr(i + 1) && id1 !== id2) {
          return id1.substr(0, i) + id1.substr(i + 1);
        }
      }
    }
  }
}

module.exports = {one, two};