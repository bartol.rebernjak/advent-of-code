const isEqual = require('lodash/isEqual');

function countSurrounding(map, x, y, char) {
  let counter = 0;
  counter += x > 0 ? map[y][x-1] === char : 0;
  counter += y > 0 ? map[y-1][x] === char : 0;
  counter += x < 49 ? map[y][x+1] === char : 0;
  counter += y < 49 ? map[y+1][x] === char : 0;
  counter += x > 0 && y > 0 ? map[y-1][x-1] === char : 0;
  counter += x < 49 && y > 0 ? map[y-1][x+1] === char : 0;
  counter += x > 0 && y < 49 ? map[y+1][x-1] === char : 0;
  counter += x < 49 && y < 49 ? map[y+1][x+1] === char : 0;
  return counter;
}

function resourceValue(map) {
  let wood = 0, lumberyards = 0;
  for (let line of map) {
    for (let char of line) {
      lumberyards += char === '#';
      wood += char === '|';
    }
  }
  return wood * lumberyards;
}

function one(input) {
  let map = [];
  for (let y = 0; y < 50; y++) {
    let row = [];
    for (let x= 0; x < 50; x++) {
      row.push(input[y][x]);
    }
    map.push(row);
  }
  for (let i = 0; i < 10; i++) {
    let newMap = [];
    for (let y = 0; y < 50; y++) {
      let newRow = [];
      for (let x = 0; x < 50; x++) {
        let newChar = map[y][x];
        if (map[y][x] === '.' && countSurrounding(map, x, y, '|') > 2) {
          newChar = '|';
        }
        if (map[y][x] === '|' && countSurrounding(map, x, y, '#') > 2) {
          newChar = '#';
        }
        if (map[y][x] === '#' && (!countSurrounding(map, x, y, '#') > 0 || !countSurrounding(map, x, y, '|') > 0)) {
          newChar = '.';
        }
        newRow.push(newChar);
      }
      newMap.push(newRow);
    }
    map = newMap;
  }
  return resourceValue(map);
}

function two(input) {
  let map = [];
  for (let y = 0; y < 50; y++) {
    let row = [];
    for (let x= 0; x < 50; x++) {
      row.push(input[y][x]);
    }
    map.push(row);
  }
  let maps = [], pattern, i;
  loop:
  for (i = 0; i < 1000000000; i++) {
    let newMap = [];
    for (let y = 0; y < 50; y++) {
      let newRow = [];
      for (let x = 0; x < 50; x++) {
        let newChar = map[y][x];
        if (map[y][x] === '.' && countSurrounding(map, x, y, '|') > 2) {
          newChar = '|';
        }
        if (map[y][x] === '|' && countSurrounding(map, x, y, '#') > 2) {
          newChar = '#';
        }
        if (map[y][x] === '#' && (!countSurrounding(map, x, y, '#') > 0 || !countSurrounding(map, x, y, '|') > 0)) {
          newChar = '.';
        }
        newRow.push(newChar);
      }
      newMap.push(newRow);
    }
    map = newMap;
    for (let n = 0; n < maps.length; n++) {
      if (isEqual(maps[n], map)) {
        pattern = maps.slice(n);
        break loop;
      }
    }
    maps.push(map);
  }
  return resourceValue(pattern[(1000000000 - i - 1) % pattern.length]);
}

module.exports = {one, two};