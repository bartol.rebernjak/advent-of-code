function toggleCase(char) {
  return char !== char.toUpperCase() ? char.toUpperCase() : char.toLowerCase();
}

function react(polymer) {
  let i = 0;
  while (i < polymer.length - 1) {
    if (polymer[i] === toggleCase(polymer[i + 1])) {
      polymer = polymer.substr(0, i) + polymer.substr(i + 2);
      i = i - 2;
    }
    i++;
  }
  return polymer;
}

function removeChar(polymer, char) {
  let pattern = `[${char.toUpperCase()}${char.toLowerCase()}]`;
  let re = new RegExp(pattern, 'g');
  return polymer.replace(re, '');
}

function one(input) {
  return react(input[0]).length;
}

function two(input) {
  let polymer = input[0], min = polymer.length;
  for (let char of "abcdefghijklmnopqrstuvwxyz") {
    let localMin = react(removeChar(polymer, char)).length;
    if (localMin < min)
      min = localMin
  }
  return min;
}

module.exports = {one, two};