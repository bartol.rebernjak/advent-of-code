function parse(input) {
  let parsed = [];
  for (let line of input) {
    parsed.push(line.split(" ")[1] + line.split(" ")[7]);
  }
  return parsed;
}

function duration(letter) {
  return letter.charCodeAt(0) - 4;
}

function countWorkers(workers) {
  return workers.filter(w => w[0] === '-').length;
}

function assignWorker(workers, letter) {
  for (let w of workers) {
    if (w[0] === '-') {
      w[0] = letter;
      w[1] = duration(letter);
      return workers;
    }
  }
  return workers;
}

function minTime(workers) {
  let min = 100;
  for (let w of workers) {
    if (w[1] < min && w[1] > -1) {
      min = w[1];
    }
  }
  return min;
}

function antecedents(input) {
  let arr = [];
  for (let item of input) {
    arr.push(item[1]);
  }
  return arr;
}

function one(input) {
  let toBeDone = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
  let rules = parse(input);
  let instructions = "";
  while (toBeDone.length) {
    for (let letter of toBeDone) {
      if (!antecedents(rules).includes(letter)) {
        instructions += letter;
        toBeDone = toBeDone.filter(e => e !== letter);
        rules = rules.filter(e => !e.startsWith(letter));
        break;
      }
    }
  }
  return instructions;
}

function two(input) {
  let toBeDone = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
  let rules = parse(input);
  let time = 0, available = [], workers =[['-', -1], ['-', -1], ['-', -1], ['-', -1], ['-', -1]];
  while (toBeDone.length || countWorkers(workers) < 5) {
    for (let letter of toBeDone) {
      if (!antecedents(rules).includes(letter)) {
        toBeDone = toBeDone.filter(e => e !== letter);
        available.push(letter);
      }
    }
    available.sort();
    while (countWorkers(workers) > 0 && available.length > 0) {
      workers = assignWorker(workers, available[0]);
      available.splice(0, 1);
    }
    time += minTime(workers);
    let timePassed = minTime(workers);
    for (let w of workers) {
      w[1] = w[1] - timePassed;
      if (w[1] === 0) {
        rules = rules.filter(e => !e.startsWith(w[0]));
        w[0] = '-';
        w[1] = -1;
      }
    }
  }
  return time;
}

module.exports = {one, two};