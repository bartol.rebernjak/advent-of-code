function getMetadataSum(nodes) {
  let children = nodes.shift();
  let metadata = nodes.shift();
  let sum = 0;
  for (let i = 0; i < children; i ++) {
    sum += getMetadataSum(nodes);
  }
  for (let i = 0; i < metadata; i ++) {
    sum += nodes.shift();
  }
  return sum;
}

function getValue(nodes) {
  let children = nodes.shift();
  let metadataCount = nodes.shift();
  if (children) {
    let childrenValues = [];
    for (let i = 0; i < children; i ++)
      childrenValues.push(getValue(nodes));
    let metadata = [];
    for (let i = 0; i < metadataCount; i ++)
      metadata.push(nodes.shift());
    let sum = 0;
    for (let m of metadata) {
      if (m - 1 >= 0 && m - 1 < childrenValues.length)
        sum += childrenValues[m - 1];
    }
    return sum;
  } else {
    let sum = 0;
    for (let i = 0; i < metadataCount; i ++)
      sum += nodes.shift();
    return sum;
  }
}

function one(input) {
  return getMetadataSum(input[0].split(' ').map(Number));
}

function two(input) {
  return getValue(input[0].split(' ').map(Number));
}

module.exports = {one, two};