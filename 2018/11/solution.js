function getPowerLevel(x, y, gsn) {
  return Math.trunc(((x + 10) * y + gsn) * (x + 10) / 100)  % 10 - 5;
}

function getTotalPower(partialSums, x, y, size) {
  let partX = x > 0 ? partialSums[x-1][y+size-1] : 0;
  let partY = y > 0 ?  partialSums[x+size-1][y-1] : 0;
  let partXY = x > 0 && y > 0 ? partialSums[x-1][y-1] : 0;
  return partialSums[x+size-1][y+size-1] + partXY - partX - partY;
}

function one(input) {
  let gsn = parseInt(input[0]);
  let partialSums = [];
  for (let i = 0; i < 300; i++) {
    let rack = [];
    for (let j = 0; j < 300; j++) {
      let prevJ = j > 0 ? rack[j-1] : 0;
      let prevI = i > 0 ? partialSums[i-1][j] : 0;
      let prevIJ = i > 0 && j > 0 ? partialSums[i-1][j-1] : 0;
      rack.push(getPowerLevel(i, j, gsn) + prevJ + prevI - prevIJ);
    }
    partialSums.push(rack);
  }
  let max =  - 45;
  let coords = '';
  for (let i = 0; i < 298; i++) {
    for (let j = 0; j < 298; j++) {
      let totalPower = getTotalPower(partialSums, i, j, 3);
      if (totalPower > max) {
        max = totalPower;
        coords = i + ',' + j;
      }
    }
  }
  return coords;
}

function two(input) {
  let gsn = parseInt(input[0]);
  let partialSums = [];
  for (let i = 0; i < 300; i++) {
    let rack = [];
    for (let j = 0; j < 300; j++) {
      let prevJ = j > 0 ? rack[j-1] : 0;
      let prevI = i > 0 ? partialSums[i-1][j] : 0;
      let prevIJ = i > 0 && j > 0 ? partialSums[i-1][j-1] : 0;
      rack.push(getPowerLevel(i, j, gsn) + prevJ + prevI - prevIJ);
    }
    partialSums.push(rack);
  }
  let max =  - 45;
  let coords = '';
  for (let k = 1; k <= 300; k++) {
    for (let i = 0; i < 301 - k; i++) {
      for (let j = 0; j < 301 - k; j++) {
        let totalPower = getTotalPower(partialSums, i, j, k);
        if (totalPower > max) {
          max = totalPower;
          coords = i + ',' + j + ',' + k;
        }
      }
    }
  }
  return coords;
}

module.exports = {one, two};