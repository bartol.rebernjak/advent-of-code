function isCombatOngoing(map) {
  let elf = false, goblin =  false;
  for (let line of map) {
    for (let e of line) {
      if (e.type && e.type === 'G') {
        if (elf)
          return true;
        goblin = true;
      }
      if (e.type && e.type === 'E') {
        if (goblin)
          return true;
        elf = true;
      }
    }
  }
  return false;
}

function one(input) {
  let map = [];
  for (let line of input) {
    let row = [];
    for (let c of line) {
      if (c !== 'G' && c !== 'E') {
        row.push(c);
        continue;
      }
      row.push({type: c, hp: 200});
    }
    map.push(row);
  }
  let round = 0;
  //while (isCombatOngoing(map)) {
  //  round += 1;
  //}
}


function two(input) {
}

module.exports = {one, two};